<?php
/**
 * File containing the Wikifab Footer class
 *
 *
 * @file
 * @ingroup   Skins
 */

namespace Skins\Chameleon\Components;

use Linker;
use Parser;

/**
 * ToolbarHorizontal class
 *
 * A horizontal toolbar containing standard sidebar items (toolbox, language links).
 *
 * The toolbar is an unordered list in a nav element: <nav role="navigation" id="p-tb" >
 *
 * @author Stephan Gambke
 * @since 1.0
 * @ingroup Skins
 */
class MovilabFooter extends Component {

	/**
	 * Builds the HTML code for this component
	 *
	 * @return String the HTML code
	 */
	public function getHtml() {
		global $wgScriptPath;

		$ret = '
		<!-- Start of Rocket.Chat Livechat Script -->
		<script type="text/javascript">
		(function(w, d, s, u) {
		w.RocketChat = function(c) { w.RocketChat._.push(c) }; w.RocketChat._ = []; w.RocketChat.url = u;
		var h = d.getElementsByTagName(s)[0], j = d.createElement(s);
		j.async = true; j.src = "https://chat.tiers-lieux.org/livechat/rocketchat-livechat.min.js?_=201903270000";
		h.parentNode.insertBefore(j, h);
		})(window, document, "script", "https://chat.tiers-lieux.org/livechat");
		</script>

		<div class="modal fade" id="ResourceModal" role="dialog" aria-labelledby="ResourceModalLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title" id="ResourceModalLabel"><span id="Créer_une_ressource"></span><span class="mw-headline" id="Cr.C3.A9er_une_ressource">Créer une ressource</span></h4>
<p><span class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></span>
</p>
</div>
<div class="modal-body">
<div class="card-columns">
    <div class="card btn collapsed" data-toggle="collapse" data-target="#PageDocumentForm">
        <div class="card-body">
          <div><i class="far fa-file"></i></div>
          <div>
              <h5 class="card-title"><span class="mw-headline" id="Document">Document</span></h5>
              <p class="card-text">Créer un document avec un fichier ou du texte</p>
          </div>
        </div>
      </div>
    <div class="card btn collapsed" data-toggle="collapse" data-target="#CommunForm">
        <div class="card-body">
          <div><i class="far fa-sun"></i></div>
          <div>
             <h5 class="card-title"><span class="mw-headline" id="Commun">Commun</span></h5>
             <p class="card-text">Mutualiser les moyens</p>
          </div>
        </div>
    </div>
    <div class="card btn collapsed" data-toggle="collapse" data-target="#TiersLieuForm">
        <div class="card-body">
           <div><i class="fas fa-store-alt"></i></div>
           <div>
              <h5 class="card-title"><span class="mw-headline" id="Tiers-lieu">Tiers-lieu</span></h5>
              <p class="card-text">Décrire un tiers-lieux</p>
           </div>
        </div>
    </div>
    <div class="card btn collapsed" data-toggle="collapse" data-target="#DefiForm">
        <div class="card-body">
          <div><i class="far fa-star"></i></div>
          <div>
             <h5 class="card-title"><span id="Défi"></span><span class="mw-headline" id="D.C3.A9fi">Défi</span></h5>
             <p class="card-text">Ajouter un défi</p>
          </div>
        </div>
    </div>
    <div class="card btn collapsed" data-toggle="collapse" data-target="#EvenementForm">
        <div class="card-body">
          <div><i class="far fa-calendar"></i></div>
          <div>
             <h5 class="card-title"><span id="Evènement"></span><span class="mw-headline" id="Ev.C3.A8nement">Evènement</span></h5>
             <p class="card-text">Raconter une rencontre</p>
         </div>
        </div>
    </div>
    <div class="card btn collapsed" data-toggle="collapse" data-target="#PageLibreForm">
        <div class="card-body">
          <div><i class="far fa-calendar"></i></div>
          <div>
             <h5 class="card-title"><span class="mw-headline" id="Page_libre">Page libre</span></h5>
             <p class="card-text">Créer une page de texte libre</p>
         </div>
        </div>
    </div>
    </div>
</div>
<div class="collapse" id="PageDocumentForm" data-parent="#ResourceModal">
	<form name="createbox" action="/wiki/Sp%C3%A9cial:AjouterPage" method="get" class="pfFormInput"><p><input type="hidden" value="Document" name="form"></p><div class="pfFormInputWrapper" data-size="" data-placeholder="Entrez ici le nom du document que vous souhaitez créer" data-autofocus="1" id="input_1" data-autocomplete-settings="Document" data-autocomplete-data-type="category" data-button-label="Créer ou modifier"><fieldset class="oo-ui-layout oo-ui-fieldsetLayout"><legend class="oo-ui-fieldsetLayout-header"><span class="oo-ui-iconElement-icon oo-ui-iconElement-noIcon"></span><span class="oo-ui-labelElement-label"></span></legend><div class="oo-ui-fieldsetLayout-group"><div class="oo-ui-layout oo-ui-horizontalLayout"><div class="pfPageNameWithoutNamespace oo-ui-widget oo-ui-widget-enabled oo-ui-inputWidget oo-ui-textInputWidget oo-ui-textInputWidget-type-text oo-ui-lookupElement" aria-disabled="false"><input type="text" tabindex="0" aria-disabled="false" class="oo-ui-inputWidget-input" name="page_name" value="" placeholder="Entrez ici le nom du document que vous souhaitez créer" autofocus="autofocus" autocomplete="off" aria-expanded="false" role="combobox" aria-owns="ooui-1" aria-autocomplete="list"><span class="oo-ui-iconElement-icon oo-ui-iconElement-noIcon"></span><span class="oo-ui-indicatorElement-indicator oo-ui-indicatorElement-noIndicator"></span><div class="oo-ui-widget oo-ui-widget-enabled oo-ui-selectWidget oo-ui-selectWidget-unpressed oo-ui-clippableElement-clippable oo-ui-floatableElement-floatable oo-ui-menuSelectWidget oo-ui-element-hidden oo-ui-lookupElement-menu" aria-disabled="false" role="listbox" id="ooui-1"></div></div><span class="pfCreateOrEditButton oo-ui-widget oo-ui-widget-enabled oo-ui-inputWidget oo-ui-buttonElement oo-ui-buttonElement-framed oo-ui-labelElement oo-ui-buttonInputWidget" aria-disabled="false"><button type="submit" tabindex="0" aria-disabled="false" class="oo-ui-inputWidget-input oo-ui-buttonElement-button" value="Submit"><span class="oo-ui-iconElement-icon oo-ui-iconElement-noIcon"></span><span class="oo-ui-labelElement-label">Créer ou modifier</span><span class="oo-ui-indicatorElement-indicator oo-ui-indicatorElement-noIndicator"></span></button></span></div></div></fieldset></div><p class="mw-empty-elt"></p></form>
</div>
<div class="collapse" id="CommunForm" data-parent="#ResourceModal">
	<form name="createbox" action="/wiki/Sp%C3%A9cial:AjouterPage" method="get" class="pfFormInput"><p><input type="hidden" value="Commun" name="form"></p><div class="pfFormInputWrapper" data-size="" data-placeholder="Entrez ici le nom du commun" data-autofocus="1" id="input_2" data-autocomplete-settings="Commun" data-autocomplete-data-type="category" data-button-label="Créer ou modifier"><fieldset class="oo-ui-layout oo-ui-fieldsetLayout"><legend class="oo-ui-fieldsetLayout-header"><span class="oo-ui-iconElement-icon oo-ui-iconElement-noIcon"></span><span class="oo-ui-labelElement-label"></span></legend><div class="oo-ui-fieldsetLayout-group"><div class="oo-ui-layout oo-ui-horizontalLayout"><div class="pfPageNameWithoutNamespace oo-ui-widget oo-ui-widget-enabled oo-ui-inputWidget oo-ui-textInputWidget oo-ui-textInputWidget-type-text oo-ui-lookupElement" aria-disabled="false"><input type="text" tabindex="0" aria-disabled="false" class="oo-ui-inputWidget-input" name="page_name" value="" placeholder="Entrez ici le nom du commun" autofocus="autofocus" autocomplete="off" aria-expanded="false" role="combobox" aria-owns="ooui-2" aria-autocomplete="list"><span class="oo-ui-iconElement-icon oo-ui-iconElement-noIcon"></span><span class="oo-ui-indicatorElement-indicator oo-ui-indicatorElement-noIndicator"></span><div class="oo-ui-widget oo-ui-widget-enabled oo-ui-selectWidget oo-ui-selectWidget-unpressed oo-ui-clippableElement-clippable oo-ui-floatableElement-floatable oo-ui-menuSelectWidget oo-ui-element-hidden oo-ui-lookupElement-menu" aria-disabled="false" role="listbox" id="ooui-2"></div></div><span class="pfCreateOrEditButton oo-ui-widget oo-ui-widget-enabled oo-ui-inputWidget oo-ui-buttonElement oo-ui-buttonElement-framed oo-ui-labelElement oo-ui-buttonInputWidget" aria-disabled="false"><button type="submit" tabindex="0" aria-disabled="false" class="oo-ui-inputWidget-input oo-ui-buttonElement-button" value="Submit"><span class="oo-ui-iconElement-icon oo-ui-iconElement-noIcon"></span><span class="oo-ui-labelElement-label">Créer ou modifier</span><span class="oo-ui-indicatorElement-indicator oo-ui-indicatorElement-noIndicator"></span></button></span></div></div></fieldset></div><p class="mw-empty-elt"></p></form>
</div>
<div class="collapse" id="DefiForm" data-parent="#ResourceModal">
	<form name="createbox" action="/wiki/Sp%C3%A9cial:AjouterPage" method="get" class="pfFormInput"><p><input type="hidden" value="Défi" name="form"></p><div class="pfFormInputWrapper" data-size="" data-placeholder="Entrez ici le nom du défi que vous souhaitez créer" data-autofocus="1" id="input_3" data-autocomplete-settings="Défi" data-autocomplete-data-type="category" data-button-label="Créer ou modifier"><fieldset class="oo-ui-layout oo-ui-fieldsetLayout"><legend class="oo-ui-fieldsetLayout-header"><span class="oo-ui-iconElement-icon oo-ui-iconElement-noIcon"></span><span class="oo-ui-labelElement-label"></span></legend><div class="oo-ui-fieldsetLayout-group"><div class="oo-ui-layout oo-ui-horizontalLayout"><div class="pfPageNameWithoutNamespace oo-ui-widget oo-ui-widget-enabled oo-ui-inputWidget oo-ui-textInputWidget oo-ui-textInputWidget-type-text oo-ui-lookupElement" aria-disabled="false"><input type="text" tabindex="0" aria-disabled="false" class="oo-ui-inputWidget-input" name="page_name" value="" placeholder="Entrez ici le nom du défi que vous souhaitez créer" autofocus="autofocus" autocomplete="off" aria-expanded="false" role="combobox" aria-owns="ooui-3" aria-autocomplete="list"><span class="oo-ui-iconElement-icon oo-ui-iconElement-noIcon"></span><span class="oo-ui-indicatorElement-indicator oo-ui-indicatorElement-noIndicator"></span><div class="oo-ui-widget oo-ui-widget-enabled oo-ui-selectWidget oo-ui-selectWidget-unpressed oo-ui-clippableElement-clippable oo-ui-floatableElement-floatable oo-ui-menuSelectWidget oo-ui-element-hidden oo-ui-lookupElement-menu" aria-disabled="false" role="listbox" id="ooui-3"></div></div><span class="pfCreateOrEditButton oo-ui-widget oo-ui-widget-enabled oo-ui-inputWidget oo-ui-buttonElement oo-ui-buttonElement-framed oo-ui-labelElement oo-ui-buttonInputWidget" aria-disabled="false"><button type="submit" tabindex="0" aria-disabled="false" class="oo-ui-inputWidget-input oo-ui-buttonElement-button" value="Submit"><span class="oo-ui-iconElement-icon oo-ui-iconElement-noIcon"></span><span class="oo-ui-labelElement-label">Créer ou modifier</span><span class="oo-ui-indicatorElement-indicator oo-ui-indicatorElement-noIndicator"></span></button></span></div></div></fieldset></div><p class="mw-empty-elt"></p></form>
</div>
<div class="collapse" id="TiersLieuForm" data-parent="#ResourceModal">
	<form name="createbox" action="/wiki/Sp%C3%A9cial:AjouterPage" method="get" class="pfFormInput"><p><input type="hidden" value="Tiers-Lieu" name="form"></p><div class="pfFormInputWrapper" data-size="" data-placeholder="Entrez ici le nom de votre tiers-lieu" data-autofocus="1" id="input_4" data-autocomplete-settings="Tiers-lieu" data-autocomplete-data-type="category" data-button-label="Créer ou modifier"><fieldset class="oo-ui-layout oo-ui-fieldsetLayout"><legend class="oo-ui-fieldsetLayout-header"><span class="oo-ui-iconElement-icon oo-ui-iconElement-noIcon"></span><span class="oo-ui-labelElement-label"></span></legend><div class="oo-ui-fieldsetLayout-group"><div class="oo-ui-layout oo-ui-horizontalLayout"><div class="pfPageNameWithoutNamespace oo-ui-widget oo-ui-widget-enabled oo-ui-inputWidget oo-ui-textInputWidget oo-ui-textInputWidget-type-text oo-ui-lookupElement" aria-disabled="false"><input type="text" tabindex="0" aria-disabled="false" class="oo-ui-inputWidget-input" name="page_name" value="" placeholder="Entrez ici le nom de votre tiers-lieu" autofocus="autofocus" autocomplete="off" aria-expanded="false" role="combobox" aria-owns="ooui-4" aria-autocomplete="list"><span class="oo-ui-iconElement-icon oo-ui-iconElement-noIcon"></span><span class="oo-ui-indicatorElement-indicator oo-ui-indicatorElement-noIndicator"></span><div class="oo-ui-widget oo-ui-widget-enabled oo-ui-selectWidget oo-ui-selectWidget-unpressed oo-ui-clippableElement-clippable oo-ui-floatableElement-floatable oo-ui-menuSelectWidget oo-ui-element-hidden oo-ui-lookupElement-menu" aria-disabled="false" role="listbox" id="ooui-4"></div></div><span class="pfCreateOrEditButton oo-ui-widget oo-ui-widget-enabled oo-ui-inputWidget oo-ui-buttonElement oo-ui-buttonElement-framed oo-ui-labelElement oo-ui-buttonInputWidget" aria-disabled="false"><button type="submit" tabindex="0" aria-disabled="false" class="oo-ui-inputWidget-input oo-ui-buttonElement-button" value="Submit"><span class="oo-ui-iconElement-icon oo-ui-iconElement-noIcon"></span><span class="oo-ui-labelElement-label">Créer ou modifier</span><span class="oo-ui-indicatorElement-indicator oo-ui-indicatorElement-noIndicator"></span></button></span></div></div></fieldset></div><p class="mw-empty-elt"></p></form>
</div>
<div class="collapse" id="EvenementForm" data-parent="#ResourceModal">
	<form name="createbox" action="/wiki/Sp%C3%A9cial:AjouterPage" method="get" class="pfFormInput">
		<p>
		<input type="hidden" value="Evenement" name="form"></p><div class="pfFormInputWrapper" data-size="" data-placeholder="Entrez ici le nom de l\'événement que vous souhaitez créer" data-autofocus="1" id="input_5" data-autocomplete-settings="Événement" data-autocomplete-data-type="category" data-button-label="Créer ou modifier"><fieldset class="oo-ui-layout oo-ui-fieldsetLayout"><legend class="oo-ui-fieldsetLayout-header"><span class="oo-ui-iconElement-icon oo-ui-iconElement-noIcon"></span><span class="oo-ui-labelElement-label"></span></legend><div class="oo-ui-fieldsetLayout-group"><div class="oo-ui-layout oo-ui-horizontalLayout"><div class="pfPageNameWithoutNamespace oo-ui-widget oo-ui-widget-enabled oo-ui-inputWidget oo-ui-textInputWidget oo-ui-textInputWidget-type-text oo-ui-lookupElement" aria-disabled="false"><input type="text" tabindex="0" aria-disabled="false" class="oo-ui-inputWidget-input" name="page_name" value="" placeholder="Entrez ici le nom de l\'événement que vous souhaitez créer" autofocus="autofocus" autocomplete="off" aria-expanded="false" role="combobox" aria-owns="ooui-5" aria-autocomplete="list"><span class="oo-ui-iconElement-icon oo-ui-iconElement-noIcon"></span><span class="oo-ui-indicatorElement-indicator oo-ui-indicatorElement-noIndicator"></span><div class="oo-ui-widget oo-ui-widget-enabled oo-ui-selectWidget oo-ui-selectWidget-unpressed oo-ui-clippableElement-clippable oo-ui-floatableElement-floatable oo-ui-menuSelectWidget oo-ui-element-hidden oo-ui-lookupElement-menu" aria-disabled="false" role="listbox" id="ooui-5"></div></div><span class="pfCreateOrEditButton oo-ui-widget oo-ui-widget-enabled oo-ui-inputWidget oo-ui-buttonElement oo-ui-buttonElement-framed oo-ui-labelElement oo-ui-buttonInputWidget" aria-disabled="false"><button type="submit" tabindex="0" aria-disabled="false" class="oo-ui-inputWidget-input oo-ui-buttonElement-button" value="Submit"><span class="oo-ui-iconElement-icon oo-ui-iconElement-noIcon"></span><span class="oo-ui-labelElement-label">Créer ou modifier</span><span class="oo-ui-indicatorElement-indicator oo-ui-indicatorElement-noIndicator"></span></button></span></div></div></fieldset></div><p class="mw-empty-elt"></p></form>
</div>
<div class="collapse" id="PageLibreForm" data-parent="#ResourceModal">
<div class="mw-inputbox-centered" style=""><form name="createbox" class="createbox" action="/index.php" method="get"><input type="hidden" value="edit" name="veaction"><input type="text" name="title" class="mw-ui-input mw-ui-input-inline createboxInput" value="" placeholder="" size="30" dir="ltr"> <input type="submit" name="create" class="mw-ui-button mw-ui-progressive createboxButton" value="Ajouter une page blanche"></form></div>
<p><br>
</p>
</div>
</div>
<div class="modal-footer">
<p><span class="btn btn-default" data-dismiss="modal">Annuler</span>
</p>
</div>
</div>
</div>
		<div class="home-support home-section">
			<div class="container">
				<div class="row">
					<div class="col-sm">
						<div class="support-block">
							<h4><span class="mw-headline" id="Soutenir_Movilab">Soutenir Movilab</span></h4>
							<p> Vous pensez que nous allons dans le bon sens&nbsp;? Si vous en avez l\'envie nous vous invitons à nous rejoindre ou à faire un don.</p>
						</div>
					</div>
					<div class="col-sm">
						<div class="support-buttons">
							<a href="/wiki/Pourquoi_contribuer_sur_ce_wiki_%3F" title="Pourquoi contribuer sur ce wiki&nbsp;?">Pourquoi s\'investir&nbsp;?</a>
							<a rel="nofollow" class="external text" href="https://opencollective.com/movilab">Faire un don</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="movilab-footer">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<h4><span class="mw-headline" id="Movilab">Movilab</span></h4>
						<ul>
							<li><a href="/wiki/Historique_de_Movilab" title="Historique de Movilab">Histoire</a></li>
							<li><a href="/wiki/C%E2%80%99est_quoi_Movilab_%3F" title="C’est quoi Movilab&nbsp;?">Raison d\'être</a></li>
							<li><a href="/wiki/La_gouvernance_de_Movilab" title="La gouvernance de Movilab">Gouvernance</a></li>
						</ul>
					</div>
					<div class="col-sm-3">
						<h4><span id="S\'impliquer"></span><span class="mw-headline" id="S.27impliquer">S\'impliquer</span></h4>
						<ul>
							<li><a href="/wiki/Contribuer_%C3%A0_Movilab" title="Contribuer à Movilab">Contribuer à Movilab</a></li>
							<li><a href="/wiki/Discuter_entre_tiers-lieux" title="Discuter entre tiers-lieux">Echanger avec la communauté</a></li>
							<li><a href="/wiki/Journal_de_bord_Movilab" title="Journal de bord Movilab">Liste des chantiers en cours</a></li>
						</ul>
					</div>
					<div class="col-sm-3">
						<h4><span class="mw-headline" id="Explorer">Explorer</span></h4>
						<ul>
							<li><a rel="nofollow" class="external text" href="https://movilab.initiative.place/wiki/Sp%C3%A9cial:WfExplore?title=Sp%C3%A9cial%3AWfExplore&amp;page=1&amp;wf-expl-Category-Document=on&amp;wf-expl-Category-Tiers-Lieu=on&amp;wf-expl-Category-Commun=on&amp;wf-expl-Category-D%C3%A9fi=on&amp;wf-expl-Category-Service=on&amp;wf-expl-Category-%C3%89v%C3%A9nement=on&amp;wf-expl-Page_creator-fulltext=&amp;wf-expl-Tags=">Découvrir les ressources</a></li>
							<li><span data-toggle="modal" data-target="#ResourceModal" style="color:white; cursor:pointer; font-size:0.9rem">Créer une ressource</span></li>
							<li><a href="/wiki/Sp%C3%A9cial:Modifications_r%C3%A9centes" title="Spécial:Modifications récentes">Modifications récentes</a></li>
						</ul>
					</div>
					<div class="col-sm-4">
						<h4><span id="Clavardons_!"></span><span class="mw-headline" id="Clavardons_.21">Clavardons&nbsp;!</span></h4>
						<ul>
							<li><a rel="nofollow" class="external text" href="https://chat.tiers-lieux.org/channel/movilab_general">Rocket Chat</a></li>
							<li><a rel="nofollow" class="external text" href="https://www.facebook.com/ConciergerieMovilab/">Facebook</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		';
		return $ret;
	}



}
