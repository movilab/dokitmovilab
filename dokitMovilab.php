<?php

$wgMessagesDirs['DokitMovilab'] = __DIR__ . "/i18n";

$wgResourceModules['ext.DokitMovilab.css'] = array(
	'styles' => array('style.css','stylev2.css'),
	'position' => 'top',
	'localBasePath' => __DIR__ . '',
	'remoteExtPath' => 'DokitMovilab',
);
$wgResourceModules['ext.DokitMovilab.js'] = array(
	'scripts' => 'style.js',
	'messages' => array(
	),
	'dependencies' => array(
		'jquery.ui'
	),
	'position' => 'bottom',
	'localBasePath' => __DIR__ . '',
	'remoteExtPath' => 'DokitMovilab',
);

$wgMessagesDirs['DokitMovilab'] = __DIR__ . '/i18n';


function DokitMovilabBeforePageDisplay( $out, $skin ) {

	$out->addModuleStyles(
		array(
			'ext.DokitMovilab.css'
		)
	);
	$out->addModules( array( 'ext.DokitMovilab.js' ) );

	return true;
}

function DokitMovilabArticleViewFooter( $article,$patrolFooterShown ) {

	global $wgOut;
	//$wgOut->addWikiTextAsInterface('{{Modèle:MovilabFooter}}');
	return true;
}


$wgHooks['BeforePageDisplay'][] = 'DokitMovilabBeforePageDisplay';
//$wgHooks['ArticleViewFooter'][] = 'DokitMovilabArticleViewFooter';

$wgAutoloadClasses['Skins\\Chameleon\\Components\\MovilabFooter'] = __DIR__ . "/chameleon-component/MovilabFooter.php";
$wgAutoloadClasses['Skins\\Chameleon\\Components\\MovilabButtonBar'] = __DIR__ . "/chameleon-component/MovilabButtonBar.php";
$wgAutoloadClasses['Skins\\Chameleon\\Components\\MovilabMainContent'] = __DIR__ . "/chameleon-component/MovilabMainContent.php";
